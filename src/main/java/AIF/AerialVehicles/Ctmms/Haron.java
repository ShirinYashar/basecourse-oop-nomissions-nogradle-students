package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;

public abstract class Haron extends Ctmm {

    private final int maxMaintenance = 15000;

    public Haron(Coordinates currentCoordinates, int maxModuleStations, Class[] acceptModules) {
        super(currentCoordinates, 15000, maxModuleStations, acceptModules);
    }

    @Override
    public void performMaintenance() {
        this.setMaintenance(this.maxMaintenance);
        System.out.println("Performing maintenance");
    }
}
