package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Shoval extends Haron {

    private static final int MAX_MODULE_STATION = 3;
    private static final Class[] ACCEPTED_MODULES = new Class[]{BDAModule.class, IntelligenceModule.class, AttackModule.class};

    public Shoval(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);
    }

}

