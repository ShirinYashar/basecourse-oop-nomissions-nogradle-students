package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;

public abstract class Hermes extends Ctmm {

    private final int maxMaintenance = 10000;

    public Hermes(Coordinates currentCoordinates, int maxModuleStations, Class[] acceptModules) {
        super(currentCoordinates, 10000, maxModuleStations, acceptModules);
    }

    @Override
    public void performMaintenance() {
        this.setMaintenance(this.maxMaintenance);
        System.out.println("Performing maintenance");
    }

}
