package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Zik extends Hermes {

    private static final int MAX_MODULE_STATION = 1;
    private static final Class[] ACCEPTED_MODULES = new Class[]{BDAModule.class, IntelligenceModule.class};

    public Zik(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);
    }

}