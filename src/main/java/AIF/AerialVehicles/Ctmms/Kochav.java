package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Kochav extends Hermes {

    private static final int MAX_MODULE_STATION = 5;
    private static final Class[] ACCEPTED_MODULES = new Class[]{BDAModule.class, IntelligenceModule.class, AttackModule.class};

    public Kochav(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);

    }

}
