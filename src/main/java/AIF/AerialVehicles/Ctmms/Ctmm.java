package AIF.AerialVehicles.Ctmms;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformInMidAirException;
import AIF.Entities.Coordinates;

public abstract class Ctmm extends AerialVehicle {

    public Ctmm(Coordinates currentCoordinates, double maintenance, int maxModuleStations, Class[] acceptModules) {
        super(currentCoordinates, maintenance, maxModuleStations, acceptModules);
    }

    public void hoverOverLocation(double hour) throws CannotPerformInMidAirException {
        if (!this.isOnLand()) {
            System.out.printf("Hovering over: <" + this.getCurrentCoordinates().getLongitude() + "," +
                    this.getCurrentCoordinates().getLatitude() + ">");
            this.setMaintenance(this.getMaintenance() - 150 * hour);
        } else {
            throw new CannotPerformInMidAirException();
        }
    }
}
