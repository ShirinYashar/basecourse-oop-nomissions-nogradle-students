package AIF.AerialVehicles.Ctmms;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.IntelligenceModule;

public class Eitan extends Haron {

    private static final int MAX_MODULE_STATION = 4;
    private static final Class[] ACCEPTED_MODULES = new Class[]{IntelligenceModule.class, AttackModule.class};

    public Eitan(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);
    }

}
