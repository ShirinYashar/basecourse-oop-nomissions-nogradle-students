package AIF.AerialVehicles.FighterJets;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;

public class F16 extends FighterJet {

    private static final int MAX_MODULE_STATION = 7;
    private static final Class[] ACCEPTED_MODULES = new Class[]{AttackModule.class, BDAModule.class};

    public F16(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);
    }

    public int getMaxModuleStations() {
        return MAX_MODULE_STATION;
    }
}
