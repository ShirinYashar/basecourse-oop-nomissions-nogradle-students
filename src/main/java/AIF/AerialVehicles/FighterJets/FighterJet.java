package AIF.AerialVehicles.FighterJets;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle {

    private static final int MAX_MAINTENANCE = 25000;

    public FighterJet(Coordinates currentCoordinates, int maxModuleStations, Class[] acceptModules) {
        super(currentCoordinates, MAX_MAINTENANCE, maxModuleStations, acceptModules);
    }

    @Override
    public void performMaintenance() {
        this.setMaintenance(MAX_MAINTENANCE);
        System.out.println("Performing maintenance");
    }
}
