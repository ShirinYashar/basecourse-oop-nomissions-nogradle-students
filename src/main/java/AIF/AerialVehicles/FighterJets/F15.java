package AIF.AerialVehicles.FighterJets;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.IntelligenceModule;


public class F15 extends FighterJet {

    private static final int MAX_MODULE_STATION = 10;
    private static final Class[] ACCEPTED_MODULES = new Class[]{AttackModule.class, IntelligenceModule.class};

    public F15(Coordinates coordinates) {
        super(coordinates, MAX_MODULE_STATION, ACCEPTED_MODULES);
    }
}
