package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.Module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AerialVehicle {

    private boolean onLand;
    private Coordinates currentCoordinates;
    private double maintenance;
    private List<Module> modules;
    private int maxModuleStations;
    private Class[] acceptModules;

    public AerialVehicle(Coordinates currentCoordinates, double maintenance,
                         int maxModuleStations, Class[] acceptModules) {
        this.setOnLand(true);
        this.setCurrentCoordinates(currentCoordinates);
        this.setMaintenance(maintenance);
        this.setModules(new ArrayList<>());
        this.setMaxModuleStations(maxModuleStations);
        this.setAcceptModules(acceptModules);
    }

    public double getMaintenance() {
        return this.maintenance;
    }

    public void setMaintenance(double maintenance) {
        this.maintenance = maintenance;
    }

    public boolean isOnLand() {
        return this.onLand;
    }

    public void setOnLand(boolean onLand) {
        this.onLand = onLand;
    }

    public Coordinates getCurrentCoordinates() {
        return this.currentCoordinates;
    }

    public void setCurrentCoordinates(Coordinates currentCoordinates) {
        this.currentCoordinates = currentCoordinates;
    }

    public List<Module> getModules() {
        return this.modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public int getMaxModuleStations() {
        return this.maxModuleStations;
    }

    public void setMaxModuleStations(int maxModuleStations) {
        this.maxModuleStations = maxModuleStations;
    }

    public Class[] getAcceptModules() {
        return this.acceptModules;
    }

    public void setAcceptModules(Class[] acceptModules) {
        this.acceptModules = acceptModules;
    }

    public void takeOff() throws CannotPerformOnGroundException, NeedMaintenanceException {

        if (this.isOnLand()) {

            if (!this.needMaintenance()) {
                System.out.println("Taking off");
                this.setOnLand(false);
            } else {
                throw new NeedMaintenanceException();
            }
        } else {
            throw new CannotPerformOnGroundException();
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformInMidAirException {

        if (!this.isOnLand()) {
            System.out.println("Flying to: <" + destination.getLongitude() + "," + destination.getLatitude() + ">");
            this.setMaintenance(this.getMaintenance() - this.getCurrentCoordinates().distance(destination));
            this.setCurrentCoordinates(destination);
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    public void land() throws CannotPerformInMidAirException {

        if (!this.isOnLand()) {
            System.out.println("Landing");
            this.setOnLand(true);
        } else {
            throw new CannotPerformInMidAirException();
        }
    }

    public boolean needMaintenance() {
        return (this.getMaintenance() <= 0);
    }

    public abstract void performMaintenance();

    public void loadModule(Module module) throws ModuleNotCompatibleException, NoModuleStationAvailableException {

        if (this.getModules().size() < this.getMaxModuleStations()) {

            if (Arrays.stream(this.getAcceptModules()).anyMatch(m -> m.getName().equals(module.getClass().getName()))) {
                this.getModules().add(module);
                System.out.println("add Module successfully: " + module.getClass().getName());
            } else {
                throw new ModuleNotCompatibleException();
            }
        } else {
            throw new NoModuleStationAvailableException();
        }
    }

    public void activateModule(Class module, Coordinates coordinates) throws NoModuleCanPerformException, ModuleNotFoundException {

        boolean wasFoundModule = false;

        for (Module moduleInList : this.getModules()) {

            if (moduleInList.getClass().getName().equals(module.getName())) {
                wasFoundModule = true;

                if (!moduleInList.activate(this.getCurrentCoordinates().distance(coordinates))) {
                    throw new NoModuleCanPerformException();
                } else {
                    if (module.equals(AttackModule.class)) {
                        this.getModules().remove(moduleInList);
                    }
                    break;
                }
            }
        }

        if (!wasFoundModule) {
            throw new ModuleNotFoundException();
        }
    }
}

