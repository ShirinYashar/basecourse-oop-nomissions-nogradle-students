package AIF.Modules;

public class AttackModule extends Module {

    private boolean wasUsed;
    private final double range = 1.5;

    public boolean isWasUsed() {
        return wasUsed;
    }

    public void setWasUsed(boolean wasUsed) {
        this.wasUsed = wasUsed;
    }

    public AttackModule() {
        this.setWasUsed(false);
    }

    @Override
    public boolean activate(double distance) {
        if (distance <= range && !this.isWasUsed()) {
            System.out.println("AttackModule active");
            this.setWasUsed(true);
            return true;
        } else {
            return false;
        }
    }

}
