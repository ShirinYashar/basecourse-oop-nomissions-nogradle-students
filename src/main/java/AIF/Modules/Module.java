package AIF.Modules;

public abstract class Module {
    public abstract boolean activate(double distance);
}
