package AIF.Modules;

public class BDAModule extends Module {

    private final double range = 0.3;

    public BDAModule() {
    }

    @Override
    public boolean activate(double distance) {
        if (distance <= range) {
            System.out.println("BDAModule active");
            return true;
        } else {
            return false;
        }

    }
}
