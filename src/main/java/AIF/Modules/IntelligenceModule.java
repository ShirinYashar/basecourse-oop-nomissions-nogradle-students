package AIF.Modules;

public class IntelligenceModule extends Module {

    private final double range = 0.6;

    public IntelligenceModule() {
    }

    @Override
    public boolean activate(double distance) {

        if (distance <= range) {
            System.out.println("IntelligenceModule active");
            return true;
        } else {
            return false;
        }
    }
}
